import { Component, OnInit } from '@angular/core';
import { Vehicule } from '../Vehicule';
import { VehiculeService } from '../vehicule.service';

@Component({
  selector: 'app-list-vehicule',
  templateUrl: './list-vehicule.component.html',
  styleUrls: ['./list-vehicule.component.css']
})
export class ListVehiculeComponent implements OnInit {

  vehicules: Vehicule[];
  modifVehicule = new Vehicule();

  selectedVehicule: Vehicule;

  // choix du véhicule
  public setSelectedVehicule(event, vehicule): void{
    if (this.modifVehicule.id !== vehicule.id){
      this.modifVehicule = vehicule;
    }
    this.selectedVehicule = vehicule;
  }

  public ajoutPlein(event, vehicule): void{
  }

  public ajoutEntretien(event, vehicule): void{
  }

  public suppressionVehicule(event, vehicule): void{
  }

  // Creation du service pour communiquer avec l'api dans le constructeur
  constructor( private vehiculeService: VehiculeService) { }

  ngOnInit(): void {

    // appel de la methode getAll() pour récupérer tous les vehicule
    this.vehiculeService.getAll().subscribe(value => {
      console.log(value);
      this.vehicules = value;
    });

    this.selectedVehicule = new Vehicule();
  }

}
