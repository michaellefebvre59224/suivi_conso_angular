import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ListVehiculeComponent } from './list-vehicule/list-vehicule.component';

import { HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import {VehiculeService} from './vehicule.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ListVehiculeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [VehiculeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
