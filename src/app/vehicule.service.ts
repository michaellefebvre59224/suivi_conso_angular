import { Injectable } from '@angular/core';
import { Vehicule} from './Vehicule';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: undefined
})
export class VehiculeService {
  // cette classe permet d'envoyer la requête a l'api, on y defini l'url et le HttpClient

  // url de la requete get, on récupère tous les vehicules
  url = 'http://localhost:8081/vehicules';

  // contructeur : on créer non HttpClient
    constructor(private http: HttpClient) {
  }

  // Methole get all pour récupérer tous les véhicules
  getAll(): Observable<Array<Vehicule>>{
      return this.http.get<Array<Vehicule>>(this.url);
  }

}
